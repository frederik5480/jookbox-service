package bot;

public enum Command {
  JOIN, LEAVE, NOW, PLAY, QUEUE, UNPAUSE, PAUSE, SKIP, VOL, VOLUME, PLAYLIST, CLEAR, NEXT;

  public static Command get(String command) {
     for (Command value : Command.values()) {
       if (value.toString().equalsIgnoreCase(command)) {
         return value;
       }
     }
     return null;
  }
}
