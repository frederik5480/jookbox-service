package bot;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.ActivityType;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.IVoiceChannel;
import sx.blah.discord.handle.obj.StatusType;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;
import sx.blah.discord.util.RequestBuffer;

/**
 * D4J AudioPlayer Example http://github.com/oopsjpeg/d4j-audioplayer/
 *
 * @author oopsjpeg
 */
public class Jookbox {

  // The token that the bot will use.
  //private static final String TOKEN = "NTA4MDAyMzkxMzY3NDgzMzk5.Dr49Tw.QbNaUBdO5_RD_tMO49rKaz67PbE";
  //private static final String BOTDISCRIMINATOR = "jookbox#1666";
  private static final String TOKEN = "NTEwMTc3ODQ0NzE0Nzk5MTE3.DsYj1Q.cdbMLdrIq2FvFzUGqHU3ZCwFmAM";
  private static final String BOTDISCRIMINATOR = "test platform#0540";
  // The prefix that the bot will use.
  private static final String PREFIX = "!";
  private static final Logger log = LoggerFactory.getLogger(Jookbox.class);
  private static IDiscordClient client;
  private final AudioPlayerManager playerManager;
  private final Map<Long, GuildMusicManager> musicManagers;

  private Jookbox() {
    musicManagers = new HashMap<>();
    playerManager = new DefaultAudioPlayerManager();
    AudioSourceManagers.registerRemoteSources(playerManager);
    AudioSourceManagers.registerLocalSource(playerManager);
  }

  public static void main(String[] args) throws DiscordException, RateLimitException {
    System.out.println("Logging bot in...");
    client = new ClientBuilder().withToken(TOKEN).build();
    client.getDispatcher().registerListener(new Jookbox());
    client.login();
    client.changePresence(StatusType.IDLE, ActivityType.PLAYING, "Nothing");
  }

  private static IVoiceChannel getVoiceChannel(IMessage message) {
    for (IVoiceChannel iVoiceChannel : message.getGuild().getVoiceChannels()) {
      for (IUser iUser : iVoiceChannel.getConnectedUsers()) {
        if (iUser.getStringID().equalsIgnoreCase(message.getAuthor().getStringID())) {
          return iVoiceChannel;
        }
      }
    }
    return null;
  }

  public static void deleteMessage(IMessage message) {
    RequestBuffer.request(() -> {
      try {
        message.delete();
      } catch (MissingPermissionsException | DiscordException e) {
        log.warn("Failed to delete message", e);
      }
      return null;
    });
  }

  private static void sendMessageToChannel(IChannel channel, String message) {
    try {
      channel.sendMessage(message);
    } catch (Exception e) {
      log.warn("Failed to send message {} to {}", message, channel.getName(), e);
    }
  }

  private void play(IMessage message, GuildMusicManager musicManager, AudioTrack track) {
    connect(true, message, false);
    musicManager.scheduler.queue(track);
  }

  @EventSubscriber
  public void onMessage(MessageReceivedEvent event) throws RateLimitException, DiscordException{
    IMessage message = event.getMessage();
    IChannel channel = message.getChannel();
    IGuild guild = message.getGuild();
    String content = message.getContent();

    // Make sure the message starts with the prefix
    if (content.startsWith(PREFIX)) {
      String[] split = content.split(" ");
      String alias = split[0].replaceFirst(PREFIX, "");
      String[] args = Arrays.copyOfRange(split, 1, split.length);
      String afterCommand = "";
      try {
        afterCommand= content.trim().split(" ", 2)[1].trim();
      }
      catch (ArrayIndexOutOfBoundsException e) {}
      GuildMusicManager musicManager = musicManagers.get(guild.getLongID());
      deleteMessage(message);
      Command command = Command.get(alias);
      assert command != null;
      switch (command) {
        case JOIN:
         connect(true, message, true);
          break;
        case LEAVE:
          connect(false, message, true);
          break;
        case NOW:
          sendMessageToChannel(channel, musicManager.player.getPlayingTrack().getInfo().title);
          break;
        case PLAY:
          if (args[0].startsWith("http")) {
            loadAndPlay(message, args[0], false);
          } else {
            loadAndPlay(message, "ytsearch:" + afterCommand, false);
          }
          break;
        case QUEUE:
          queue(afterCommand, musicManager, channel);
          break;
        case UNPAUSE:
          pause(false, message, musicManager);
          break;
        case PAUSE:
          pause(true, message, musicManager);
          break;
        case SKIP:
          skipTrack(channel);
          break;
        case VOL:
        case VOLUME:
          try {
            musicManager.player.setVolume(Integer.parseInt(args[0]));
          } catch (NumberFormatException e) {
            channel.sendMessage("Invalid volume percentage.");
          }
          break;
        case PLAYLIST:
          loadAndPlay(message, args[0], true);
          break;
        case CLEAR:
          clearQueue(musicManager, message);
          break;
        case NEXT:
          next(musicManager, message);
          break;
        default:
          sendMessageToChannel(channel, alias + " is not a supported command");
          break;
      }
    }
  }

  private void next(GuildMusicManager musicManager, IMessage message) {
    sendMessageToChannel(message.getChannel(), "Next in queue is: " + musicManager.player.getPlayingTrack().getInfo().title);
  }

  private void clearQueue(GuildMusicManager musicManager, IMessage message) {
    musicManager.scheduler.clearQueue();
    sendMessageToChannel(message.getChannel(), "Queue was just cleared by " + message.getAuthor().getName());
  }

  private void queue(String afterCommand, GuildMusicManager musicManager, IChannel channel) {
    StringBuilder builder = new StringBuilder();
    Integer[] currentTrack = {1};
    int start = 0;
    if (StringUtils.isNumeric(afterCommand)) {
      start = Integer.parseInt(afterCommand);
    }
    boolean[] stop = {false};
    int finalStart = start;
    musicManager.scheduler.getQueue().forEach(track -> {
      if (stop[0]) {
        return;
      }
      if (builder.length() > 1700) {
        builder.append("and so on, total of playlist is ")
            .append(musicManager.scheduler.getQueue().size())
            .append("..., if you want to see more specify the number to start listing from after the command.");
        sendMessageToChannel(channel, builder.toString());
        stop[0] =true;
        return;
      }
      if (currentTrack[0] > finalStart - 1) {
          builder.append(currentTrack[0]).append(". ").append(track.getInfo().title)
              .append("\n");
      }
      currentTrack[0]++;
    });
  }

  private void connect(boolean connect, IMessage message, boolean sendMessage) {
    IVoiceChannel voice = getVoiceChannel(message);
    if (connect) {
      if (voice == null) {
        if (sendMessage) {
          sendMessageToChannel(message.getChannel(),
              message.getAuthor().getName() + " is not in a voice channel.");
        }
        return;
      }
      voice.join();
      if (sendMessage) {
        sendMessageToChannel(message.getChannel(), "Connected to **" + voice.getName() + "**.");
      }
    } else {
      // Modify in the future if there's more guilds using this bot as this might get heavy.
      message.getGuild().getVoiceChannels().forEach(channel -> channel.getConnectedUsers().forEach(iUser -> {
        if ((iUser.getName() + "#" + iUser.getDiscriminator()).equals(BOTDISCRIMINATOR)) {
          channel.leave();
          message.getChannel().sendMessage("Disconnected from **" + channel.getName() + "**.");
        }
      }));
    }
  }

  private void loadAndPlay(IMessage message, String trackUrl, boolean fullPlaylist) {
    GuildMusicManager musicManager = getGuildAudioPlayer(message.getGuild());
    playerManager.loadItemOrdered(musicManager, trackUrl, new AudioLoadResultHandler() {
      @Override
      public void trackLoaded(AudioTrack track) {
        sendMessageToChannel(message.getChannel(), "Adding to queue " + track.getInfo().title);
        play(message, musicManager, track);
      }

      @Override
      public void playlistLoaded(AudioPlaylist playlist) {
        if (fullPlaylist) {
          Integer[] i = {0};
          playlist.getTracks().forEach(track -> {
            if (i[0] < 3) {
              sendMessageToChannel(message.getChannel(),
                  "Adding to queue " + track.getInfo().title + " (from playlist " + playlist
                      .getName() + ")");
            } else if (i[0] == 3) {
              sendMessageToChannel(message.getChannel(), "...and the rest of the playlist, Discord stops me from spamming every song."
                  + "\nUse !queue to see the the rest of the added songs.");
            }
            i[0]++;
            play(message, musicManager, track);
          });
        } else {
          AudioTrack firstTrack = playlist.getSelectedTrack();
          if (firstTrack == null) {
            firstTrack = playlist.getTracks().get(0);
          }
          sendMessageToChannel(message.getChannel(),
              "Adding to queue " + firstTrack.getInfo().title + " (first track of playlist "
                  + playlist.getName() + ")");
          play(message, musicManager, firstTrack);
        }
      }

      @Override
      public void noMatches() {
        sendMessageToChannel(message.getChannel(), "Nothing found by " + trackUrl);
      }

      @Override
      public void loadFailed(FriendlyException exception) {
        sendMessageToChannel(message.getChannel(), "Could not play: " + exception.getMessage());
      }
    });
  }

  private synchronized GuildMusicManager getGuildAudioPlayer(IGuild guild) {
    long guildId = guild.getLongID();
    GuildMusicManager musicManager = musicManagers.get(guildId);
    if (musicManager == null) {
      AudioPlayer player = playerManager.createPlayer();
      musicManager = new GuildMusicManager(player, new TrackScheduler(player) {
        @Override
        public void onTrackStart(AudioPlayer player, AudioTrack track) {
          client.changePresence(StatusType.ONLINE, ActivityType.PLAYING, track.getInfo().title);
        }

        @Override
        public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {
          client.changePresence(StatusType.IDLE, ActivityType.PLAYING, "Nothing");
          // Only start the next track if the end reason is suitable for it (FINISHED or LOAD_FAILED)
          if (endReason.mayStartNext) {
            nextTrack();
          }
        }
      });
      musicManagers.put(guildId, musicManager);
    }
    guild.getAudioManager().setAudioProvider(musicManager.getAudioProvider());
    return musicManager;
  }

  private void skipTrack(IChannel channel) {
    GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
    musicManager.scheduler.nextTrack();
    sendMessageToChannel(channel, "Skipped to next track.");
  }

  private void pause(boolean pause, IMessage message, GuildMusicManager manager) {
    manager.player.setPaused(pause);
    String paused = pause ? "paused" : "unpaused";
    sendMessageToChannel(message.getChannel(), message.getAuthor().getName() + " just " + paused + " the music.");
  }
}